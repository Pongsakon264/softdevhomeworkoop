import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class tableUnittest {
	@Test
	public void testRow1Win() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o, x);
		table.setRowCol(1, 1);
		table.setRowCol(1, 2);
		table.setRowCol(1, 3);
		assertEquals(true, table.checkWin());
	}

	public void testRow2Win() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o, x);
		table.setRowCol(2, 1);
		table.setRowCol(2, 2);
		table.setRowCol(2, 3);
		assertEquals(true, table.checkWin());
	}
	
	public void testRow3Win() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o, x);
		table.setRowCol(3, 1);
		table.setRowCol(3, 2);
		table.setRowCol(3, 3);
		assertEquals(true, table.checkWin());
	}
	
	public void testTayang1Win() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o, x);
		table.setRowCol(1, 1);
		table.setRowCol(2, 2);
		table.setRowCol(3, 3);
		assertEquals(true, table.checkWin());
	}
	
	public void testTayang2Win() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o, x);
		table.setRowCol(1, 3);
		table.setRowCol(2, 2);
		table.setRowCol(3, 1);
		assertEquals(true, table.checkWin());
	}
}