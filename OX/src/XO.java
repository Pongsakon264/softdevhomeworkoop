import java.util.*;

public class XO {
	static char turn = 'O';
	static char[][] XOgame = { { '-', '-', '-' }, 
	{ '-', '-', '-' }, { '-', '-', '-' } };
	static int count = 0;

	private static void welcomeFunc() {
		System.out.println("***** Welcome to XO GAME *****");
	}

	private static void tableFunc() {
		System.out.println("  1   2   3");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print("| ");
				System.out.print(XOgame[i][j] + " ");
			}
			System.out.print("|");
			System.out.println();
		}

	}

	private static void turnFunc() {
		System.out.println("Turn : " + turn);
	}

	public static void inputFunc(int row , int col) {
		Scanner kb = new Scanner(System.in);
		System.out.println("Please input row and collum : ");
		row = kb.nextInt();
		col = kb.nextInt();
		count++;
		if (ErrorFunc(row,col) == true) {
			inputFunc(row,col);
		} else {
			XOgame[row][col] = turn;
		}
	}

	public static void checkTurn() {
		if (turn == 'O') {
			turn = 'X';
		} else if (turn == 'X') {
			turn = 'O';
		}
	}

	private static boolean ErrorFunc(int row,int col) {
		if (row > 2 || row < 0) {
			System.out.println("ERROR , Please input 0 or 1 or 2");
			return true;
		} else if (col > 2 || col < 0) {
			System.out.println("ERROR , Please input 0 or 1 or 2");
			return true;
		} else if (XOgame[row][col] == 'O' || XOgame[row][col] == 'X') {
			System.out.println("Please pick another Row and Collum");
			return true;
		} else {
			return false;
		}
	}

	private static boolean checkwinHorizontal() {
		for (int r = 0; r < 3; r++) {
			if (XOgame[r][0] == turn && XOgame[r][1] == turn && XOgame[r][2] == turn) {
				showWin();
				return true;
			}
		}
		return false;
	}

	private static boolean checkwinVertical() {
		for (int r = 0; r < 3; r++) {
			if (XOgame[0][r] == turn && XOgame[1][r] == turn && XOgame[2][r] == turn) {
				showWin();
				return true;
			}
		}
		return false;
	}

	private static boolean checkwinDiagonalleft() {
		for (int r = 0; r < 3; r++) {
			if (XOgame[0][0] == turn && XOgame[1][1] == turn && XOgame[2][2] == turn) {
				showWin();
				return true;
			}
		}
		return false;
	}

	private static boolean checkwinDiagonalright() {
		for (int r = 0; r < 3; r++) {
			if (XOgame[0][2] == turn && XOgame[1][1] == turn && XOgame[2][0] == turn) {
				showWin();
				return true;
			}
		}
		return false;
	}

	private static boolean checkDraw() {
		if (count == 9) {
			System.out.println("  1   2   3");
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					System.out.print("| ");
					System.out.print(XOgame[i][j]);
					System.out.print(" ");
				}
				System.out.print("| ");
				System.out.println();
			}
			System.out.println("DRAW , play it again.");
			return true;
		}
		return false;
	}

	private static void showWin() {
		System.out.println("  1   2   3");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print("| ");
				System.out.print(XOgame[i][j]);
				System.out.print(" ");
			}
			System.out.print("|");
			System.out.println();
		}
		System.out.println(turn + " Win!!!!!");
		System.out.println("Thank you for play this game.");
		System.out.println("See you soon !");
	}

	public static boolean checkWin() {
		if (checkwinDiagonalright() == true || checkwinDiagonalleft() == true) {
			return true;
		} else if (checkwinHorizontal() == true || checkwinVertical() == true) {
			return true;
		} else if (checkDraw() == true) {
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		int row = 0;
		int col = 0;
		welcomeFunc();
		for (;;) {
			tableFunc();
			turnFunc();
			inputFunc(row,col);
			if (checkWin() == true) {
				break;
			}
			checkTurn();
		}
	}
}